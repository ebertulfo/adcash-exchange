var assert = require('chai').assert;
var chai = require('chai');
var expect = require('chai').expect;
var should = require('chai').should();
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised).should();
var  MongoClient = require('mongodb').MongoClient;

var dbUrl = "mongodb://adcash:password123@ds245971.mlab.com:45971/exchange";
var exchange = require('./exchange');
var db = null;
describe('The Exchange Class:', function () {
    before(function (done) {
        MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, client) => {
            assert.equal(null, err);
            db = client.db()
            exchange = new exchange(db);
        
            done();            
        })
    });
    describe('The getParams() function', function(){
        it('should reject if country parameter is missing', function(){
            var params = {category: 'automobile', bid: 1};
            return assert.isRejected(exchange.getParams(params));
        })
        it('should reject if category parameter is missing', function(){
            var params = {country: 'US', bid: 1};
            return assert.isRejected(exchange.getParams(params));
        })
        it('should reject if bid parameter is missing', function(){
            var params = {country: 'US', category: 'automobile'};
            return assert.isRejected(exchange.getParams(params));
        })
        it('should resolve with the params if parameters are correct', function(){
            var params = {country: 'US', category: 'automobile', bid: 1};
            return exchange.getParams(params).should.eventually.equal(params);
        })
    })

    describe('The baseTageting() function', function(){
        it('should reject if country parameter is missing', function(){
            var params = {category: 'automobile', bid: 1};
            return assert.isRejected(exchange.baseTargeting(params));
        })
        it('should reject if category parameter is missing', function(){
            var params = {country: 'US', bid: 1};
            return assert.isRejected(exchange.baseTargeting(params));
        })
        
        it('should reject if country has no match', function(){
            var params = {country: 'PH', category: 'automobile', bid: 1};
            return assert.isRejected(exchange.baseTargeting(params));
        })

        it('should reject if category has no match', function(){
            var params = {country: 'US', category: 'agriculture', bid: 1};
            return assert.isRejected(exchange.baseTargeting(params));
        })

        it('should resolve with the passing companies and parameters if category has no match', function(){
            var params = {country: 'US', category: 'automobile', bid: 1};
            return exchange.baseTargeting(params).should.eventually.be.an('object').that.have.all.keys('companies', 'params');
        })
    })

    describe("The budgetCheck() function", function() {
        it('should reject if data object does not have companies element', function(){
            var params = {params: {country: 'US', category: 'automobile', bid: 1}};
            return assert.isRejected(exchange.budgetCheck(params));
        })
        it('should reject if data object does not have params element', function(){
            var params = {companies: [{name: 'C1'}]};
            return assert.isRejected(exchange.budgetCheck(params));
        })
        it('should reject if companies element is not an array of objects', function(){
            var params = {companies: "C1", params:{country: 'US', category: 'automobile', bid: 1}};
            return assert.isRejected(exchange.budgetCheck(params));
        })
        it('should reject if company budget minus the bid param is less than 0', function(){
            var params = {companies: [{budget: 2}], params:{bid: 10}};
            return assert.isRejected(exchange.budgetCheck(params));
        })
        it('should accept if company budget minus the bid param is more than or equal to 0', function(){
            var params = {companies: [{budget: 2}], params:{bid: 1}};
            return assert.isFulfilled(exchange.budgetCheck(params));
        })
    
        it('should accept and return companies that passes the budget check', function(){
            var valid = {name: "C1", budget: 2};
            var params = {companies: [valid, {name: "C2", budget: 1}], params:{bid: 2}};
            return exchange.budgetCheck(params).should.eventually.be.an('object').that.have.property("companies").that.deep.equals([valid]);
        })
    });

    describe("the baseBidCheck() function", function() {
        it('should reject if data object does not have companies element', function(){
            var params = {params: {country: 'US', category: 'automobile', bid: 1}};
            return assert.isRejected(exchange.baseBidCheck(params));
        })
        it('should reject if data object does not have params element', function(){
            var params = {companies: [{name: 'C1'}]};
            return assert.isRejected(exchange.baseBidCheck(params));
        })
        it('should reject if companies element is not an array of objects', function(){
            var params = {companies: "C1", params:{country: 'US', category: 'automobile', bid: 1}};
            return assert.isRejected(exchange.baseBidCheck(params));
        })
        it('should reject if the bid param is less than the company base bid', function(){
            var params = {companies: [{bid: 1}], params:{bid: 0.1}};
            return assert.isRejected(exchange.baseBidCheck(params));
        })
        it('should accept if the bid param is more than or equal to the company base bid', function(){
            var params = {companies: [{bid: 1}], params:{bid: 1}};
            return assert.isFulfilled(exchange.baseBidCheck(params));
        })
    
        it('should accept and return companies that passes the base bid check', function(){
            var valid = {name: "C1", "bid": 1};
            var params = {companies: [valid, {name: "C2", bid: 2}], params:{bid: 1}};
            return exchange.baseBidCheck(params).should.eventually.be.an('object').that.have.property("companies").that.deep.equals([valid]);
        })
    });

    describe("the shortlisting() function", function() {
        it('should reject if data object does not have companies element', function(){
            var params = {};
            return assert.isRejected(exchange.shortlisting(params));
        })

        it('should return the company with the highest base bid', function(){
            var winner = {companyId:"C3", bid: 2};
            var params = {companies: [{companyId:"C1", bid: 1}, {companyId:"C2", bid: 1}, winner]};
            return exchange.shortlisting(params).should.eventually.equals(winner);
        })
    })
    
    describe("Integration test:", function() {
        
        
        it("should be rejected with No Companies Passed from Targeting message", function() {
            var expected = "No Companies Passed from Targeting"
            var params = {country: "US", category: "agriculture", bid: 10};
            return exchange.match(params).should.be.rejectedWith(expected);
        })
        
        it("should be rejected with No Companies Passed from Targeting", function() {
            var expected = "No Companies Passed from Targeting"
            var params = {country: "PH", category: "it", bid: 10};
            return exchange.match(params).should.be.rejectedWith(expected);
        })
        
        it("should be rejected with No Companies Passed from Budget", function() {
            var expected = "No Companies Passed from Budget";
            var params = {country: "US", category: "it", bid: 10};
            return exchange.match(params).should.be.rejectedWith(expected);
        })
        expected = "C2"
        it("should return "+ expected, function() {
            var params = {country: "US", category: "it", bid: 1};
            return exchange.match(params).should.eventually.equals(expected);
        })
    })


});



