
const cluster = require('cluster');
const Api = require("./api");

if (cluster.isMaster) {
    let cpus = require('os').cpus().length;
    for (let i = 0; i < cpus; i += 1) {
        cluster.fork();
    }    
    cluster.on('exit', function (worker) {
        console.log(`worker ${worker.id} exited, respawning...`);
        cluster.fork();
    });    
} else {
    let instance = new Api;
    instance.start();
}