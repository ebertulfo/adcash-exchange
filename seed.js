let MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    dbUrl = "mongodb://adcash:password123@ds245971.mlab.com:45971/exchange";

MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, client) => {
    assert.equal(null, err);
    
    const db = client.db();  
    
    let companies = [
        {
            companyId: "C1",
            countries: [
                "US",
                "FR"
            ],
            budget: "1$",
            bid: "10c",
            categories: [
                "automobile",
                "finance"
            ]
        },
        {
            companyId: "C2",
            countries: [
                "IN",
                "US"
            ],
            budget: "2$",
            bid: "30c",
            categories: [
                "it",
                "finance"
            ]
        },
        {
            companyId: "C3",
            countries: [
                "US",
                "RU"
            ],
            budget: "3$",
            bid: "5c",
            categories: [
                "automobile",
                "it"
            ]
        }
    ]

    db.collection("companies").remove();

    db.collection("companies")
        .insert(companies)
        .then((err, ret) => {
            console.log("Database Seeded!")
            process.exit();
        });
})
