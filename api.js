const cluster = require('cluster')
    express = require('express'),
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    exchange = require('./exchange'),
    config = require('./config');
        dbUrl = `${config.database.url}:${config.database.port}/${config.database.db}`;
        app = express(),
        port = 3001,        
        // This is just to set a random amount that is larger than 10000 as the number of requests
        // a worker can accommodate before getting dying. You can always just put a number as the Worker's Health.
        // This a way of preventing memory leaks.
        counter = 10000 + Math.round(Math.random() * 10000); 
class Api {
    start() {
        MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, client) => {
            assert.equal(null, err);
            console.log("Connected to Database");
            
            const db = client.db();  
            app.get('/', (req, res) => {
                console.log(req.query)
                let x = new exchange(db);
                x.match(req.query).then((result => {
                    console.log(result);
                    res.send(result);
                }),(err) => {
                    console.log(err);  
                    res.send(err);
                })
                if (counter-- === 0) {
                    cluster.worker.disconnect();
                }
            });
            app.listen(port);
            console.log(`worker ${cluster.worker.id} is listening on port ${port}...`);
        })
        
    }
            
}


module.exports =  Api;