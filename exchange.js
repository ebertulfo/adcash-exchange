const Logger = require("./logger");

class Exchange {
    constructor(db) {
        this.db = db;
        this.baseTargeting = this.baseTargeting.bind(this);
        this.budgetCheck = this.budgetCheck.bind(this);
        this.baseBidCheck = this.baseBidCheck.bind(this);
        this.shortlisting = this.shortlisting.bind(this);
        this._toCurrency = this._toCurrency.bind(this);
        this._toNumeric = this._toNumeric.bind(this);
        this.logger = new Logger;
    }
    match(params) {
        return new Promise((resolve, reject) => {
            this.getParams(params)
                .then(this.baseTargeting, this.handleError)
                .then(this.budgetCheck, this.handleError)
                .then(this.baseBidCheck, this.handleError)
                .then(this.shortlisting, this.handleError)
                .then((company)=> {
                    resolve(company.companyId);
                    this.updateBudget(company, params.bid)
                        .then((data) => {
                            console.log("Transaction Saved.");
                        }, reject);
                })
                .catch(reject);
                this.logger.log("\n")
        });
    }
    
    handleError(err) {
        return Promise.reject(err)
    }

    getParams(params) {
        return new Promise((resolve, reject) => {
            //Check if required params are present
            let requiredKeys = ['country', 'category', 'bid'];
            let keys = Object.keys(params);
            for(let requiredKey of requiredKeys) {
                if(!keys.includes(requiredKey)) {
                    reject("Missing required key: "+requiredKey);
                    break;
                }
            }
            params.bid = this._toNumeric(params.bid);
            resolve(params);
        });
    }

    updateBudget(company, bid) {
        return new Promise((resolve, reject) => {
            this.db.collection("companies")
            .update({_id: company._id}, {$set: {budget: this._toCurrency((this._toNumeric(company.budget) - bid).toFixed(2), "$")}})
            .then((ret) => {
                resolve(ret);
            })
            .catch((err) => {
                reject(err);
            });
        })
            
    }

    baseTargeting(params) {
        return new Promise((resolve, reject) => {
            this.getAllCompanies().toArray((err, docs) => {
                let passed = [];
                let failed = [];
                this.logger.log("BaseTargeting:")
                for (let company of docs) {
                    if(typeof company.countries == 'undefined' || typeof company.categories == 'undefined' ) {
                        continue;
                    }
                    if(company.countries.includes(params.country) && company.categories.includes(params.category)) {
                        passed.push(company);
                        this.logger.log(`{${company.companyId}, Passed},`)
                    } else {
                        failed.push(company);
                        this.logger.log(`{${company.companyId}, Failed},`)
                    }
                }
                
                if(passed.length != 0) {
                    resolve({companies: passed, params: params});
                } else {
                    reject("No Companies Passed from Targeting");    
                }
            });
        });
        
    }

    budgetCheck(data) {
        return new Promise((resolve, reject) => {
            let passed = [];
            let failed = [];
            this.logger.log("BudgetCheck:")
            for (let company of data.companies) {
                if(this._toNumeric(company.budget) - data.params.bid >= 0) {
                    passed.push(company);
                    this.logger.log(`{${company.companyId}, Passed},`)
                } else {
                    failed.push(company);
                    this.logger.log(`{${company.companyId}, Failed},`)
                }
            }
            if(passed.length != 0) {
                resolve({companies: passed, params: data.params});
            } else {
                reject("No Companies Passed from Budget");
                
            }
        })
    }

    baseBidCheck(data) {
        return new Promise((resolve, reject) => {
            if(typeof data.companies == undefined || typeof data.params == undefined ) {
                resolve("Invalid parameters.");
            }
            let passed = [], 
                failed = [];

            try {
                this.logger.log("BaseBid:")
                for (let company of data.companies) {
                    if(this._toNumeric(data.params.bid)  >= this._toNumeric(company.bid)) {
                        passed.push(company);
                        this.logger.log(`{${company.companyId}, Passed},`)
                    } else {
                        failed.push(company);
                        this.logger.log(`{${company.companyId}, Failed},`)
                    }
                }
            } catch (e) {
                reject(e);
            }
            
            if(passed.length != 0) {
                resolve({companies: passed, params: data.params});
            } else {
                reject("No Companies Passed from BaseBid check");
                
            }
        })
    }

    shortlisting(data) {
        return new Promise((resolve, reject) => {
            const max = data.companies.reduce((prev, current) => {
                return (this._toNumeric(prev.bid) > this._toNumeric(current.bid)) ? prev : current
            }) //returns object
            this.logger.log(`Winner = ${max.companyId}`)
            resolve(max);
        })
    }

    getAllCompanies() {
        return this.db.collection("companies")
            .find();
    }

    _toNumeric(value) {
        if(typeof value !== "number") {
            if( value.toString().toLowerCase().indexOf("c") > -1 ) {
                return  (value.replace(/[^\d.-]/g,'') / 100);
            } else if ( value.indexOf("$") > -1 ) {
                return value.replace(/[^\d.-]/g,'');
            } else {
                return parseFloat(value);
            }
        } else {
            return value;
        }
        
    }

    _toCurrency(value, currency) {
        value = currency.toLowerCase() == 'c' ? value * 100 : value;
        return value+currency;
    }

}

module.exports = Exchange;