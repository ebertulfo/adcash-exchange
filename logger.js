const fs = require('fs');

class Logger {
    log(text) {
        fs.open('./log.txt', 'a', function( e, id ) {
            fs.write( id, text, null, 'utf8', function(){
                fs.close(id, function() {})
            });
        });
    }
}

module.exports = Logger;